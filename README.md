# kubernetes-demo

## Your local kubernetes (3 ways)
  * minikube

    ```sh
    # launch cmd
    minikube start
    minikube addons enable ingress
    ```

  * kind (Kubernetes in docker)
[kind](https://kind.sigs.k8s.io/docs/user/quick-start/) is a tool     for running local Kubernetes clusters using     Docker container “nodes”.

    ```sh
    # launch cluster cmd
    kind create cluster --config kind-example-config.yaml
    ```
    
    *kind-example-config.yaml*
    ```yaml 
    # three node (two workers) cluster config
    kind: Cluster
    apiVersion: kind.x-k8s.io/v1alpha4
    nodes:
    - role: control-plane
      kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
            authorization-mode: "AlwaysAllow"
      extraPortMappings:
      - containerPort: 80
        hostPort: 80
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        protocol: TCP
    - role: worker
    - role: worker
    ```



  * kubernetes from docker for mac
    
    Used kubenetes for docker daemon
    Click on the Docker icon and go to Preferences window.
    
    Click on the Kubernetes icon.
    Simply check on the Enable Kubernetes option and then hit the Apply button

## kubernetes-commandes

### Create NS
  ```
  kubectl create ns demo
  ```

  place yourself in your namespace
  ```sh
  # do it alone on minikubes
  kubectl config set-context minikube --namespace demo
  # or use tool 
  kubens demo
  ```

### List available resources
```
kubectl api-resources
```

### First deploy

```
kubectl apply -f demo.yaml
```
#### check what happen
```
kubectl get all
```

### real deploy
kubectl get deploy demo-app -o yaml

**Tips** dry-run is your friend  
`kubectl create deploy test --image=alpine --dry-run -o yaml`  
edit your conf your ready to go
`kubectl apply -f test.yaml`


### First rolling update
run folowing cmd 
```
kubectl set image deploy/demo-app demo-app=cyparisot/side-project:demo2
```
Or edit demo.yaml and run `kubectl apply -f demo.yaml`
#### check what happen
```
kubectl get rs -o wide
kubectl get deploy demo-app -o yaml
kubectl get all
kubectl describe deploy/demo-app
```

### first scale
```
kubectl scale --replicas=4 deploy/demo-app
```
#### check what happen
```
kubectl get rs -o wide
kubectl get pods
```

### First service
```
kubectl expose deploy demo-app --port=80 --target-port=8080
```

#### check what happen
```
kubectl get svc
```

### first ingress
```sh
#Activate ingress controller
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
```


```
kubectl apply -f ingress-demo.kind.yaml
```

**WARN DNS for minikube exemple**
```
echo "$(minikube ip) yolo.io" | sudo tee -a /etc/hosts
```

#### check what happen
```
kubectl get ing
```


### first deploy ZDD

```sh
# on prompt1
while true; do sleep 1; curl http://localhost/; echo -e '\n'$(date);done
# on prompt2
kubectl set image deploy/demo-app demo-app=cyparisot/side-project:demo
```

#### check what happen
```
kubectl get all -o wide
```

#### Use configMap (optional)
- create cm
```
kubectl create configmap myapp-config --from-file=app/static/index.html
```
- update demo **_(now using real life call)_**
```
kubectl apply -f demo.yaml
```
- patch
```
kubectl patch deploy demo-app -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```
## ANNEXES
ingress-demo.yaml
demo.yaml*

## Bonus
- dashboard `minikube dashboard`
- port forward
```
kubectl --namespace demo port-forward $(kubectl --namespace demo get pod -l run=demo-app -o template --template="{{(index .items 0).metadata.name}}") 8080
```


## Bonus, question
if we scale a RS? Why does not this work ?

## clean all
```
kubectl delete ns demo
```
